\documentclass[10pt, a4paper, abstract=true, bibliography = totocnumbered]{scrartcl}

\usepackage{packages}
\usepackage{basics}
\bibliography{references.bib}

\title{Rigid Geometry via Formal Schemes}
\subtitle{Raynaud's Theorem}
\author{}
\date{}

\begin{document}

    \maketitle

    \begin{abstract}
        In this talk we will see that the functor $\rig$ induces an equivalence between the category of quasiparacompact admissible formal schemes up to admissible formal blowing ups and the category of quasiparacompact and quasiseparated rigid spaces.
    \end{abstract}

    \section{Localization of categories}

    In this section we introduce the concept of localization for categories.

    \begin{definition}\label{def:loc-cats}
        Let $\calC$ be a category and let $S$ be a class of maps in $\calC$.
        Then the \emph{localization of $\calC$ at $S$} is the pair $(\calC[S^{-1}], i)$, consisting of a category $\calC[S^{-1}]$ and a functor $i \colon \calC \to \calC[S^{-1}]$, that is uniquely (up to unique isomorphism of categories) determined by the following properties:
        \begin{itemize}
            \item
            For every $s \in S$ the map $i(s)$ in $\calC[S^{-1}]$ is an isomorphism.

            \item
            Suppose that $F \colon \calC \to \calD$ is a functor such that $F(s)$ is an isomorphism in $\calD$ for all $s \in S$.
            Then there exists a unique functor $\widetilde{F} \colon \calC[S^{-1}] \to \calD$ such that $F = \widetilde{F} i$.
        \end{itemize}
        We will usually leave the localization functor $i$ implicit in our notation.
    \end{definition}

    \begin{remark}
        We make the following remarks about \cref{def:loc-cats}:
        \begin{itemize}
            \item
            We do not claim that the localization of a category at some class of maps always exists.
            Localizations of small categories always exist but in general things might be subtle.
            I am not so sure what is true and what is not true but at least the localization of a locally small category might not be locally small again.

            \item
            There is also a $2$-categorical notion of localization of categories that is slightly different from our definition (in particular the localization in this sense will only be well defined up to equivalence of categories).
        \end{itemize}
    \end{remark}

    \begin{definition} \label{def:right-mult-sys}
        Let $\calC$ be a category and let $S$ be a class of maps in $\calC$.
        Then $S$ is called a \emph{right multiplicative system} if the following conditions are satisfied:
        \begin{itemize}
            \item
            For all $x \in \calC$ we have $\id_x \in S$.

            \item
            Suppose that $s \colon y \to x$ and $t \colon z \to y$ are elements of $S$.
            Then there exists a map $f \colon z' \to z$ such that the composition $stf$ is also contained in $S$.

            \item
            Suppose that $s \colon x' \to x$ is an element of $S$ and that $f \colon y \to x$ is some map in $\calC$.
            Then there exists an object $y' \in \calC$ and two maps $s' \colon y' \to y$ and $f' \colon y' \to x'$ such that $s' \in S$ and $sf' = fs'$.

            \item
            Suppose that $s \colon w \to x$ is contained in $S$ and that we are given two maps $f,g \colon x \to y$ in $\calC$ such that $fs = gs$.
            Then there exists a map $t \colon y \to z$ contained in $S$ such that $tf = tg$.

            \item
            For every $x \in \calC$ the class
            \[
                \set[\big]{s \in S}{\text{$s$ has target $x$}}/\cong
            \]
            is a set (here, \enquote{$\cong$} denotes the relation of being isomorphic in the over-category $\calC_{/x}$).
        \end{itemize}
    \end{definition}

    \begin{lemma}\label{lem:loc-exists}
        Let $\calC$ be a category and let $S$ be a right multiplicative system in $\calC$.
        Then the localization of $\calC$ exists and has the following explicit description:
        \begin{itemize}
            \item
            The objects of $\calC[S^{-1}]$ are the same as the objects of $\calC$ (via the localization functor).

            \item
            Every map $x \to y$ in $\calC[S^{-1}]$ is of the form $fs^{-1}$ for some maps $x \xleftarrow{s} z \xrightarrow{f} y$ in $\calC$ with $s \in S$.

            \item
            Suppose we are given two pairs of maps $x \xleftarrow{s} z \xrightarrow{f} y$ and $x \xleftarrow{s'} z' \xrightarrow{f'} y$ in $\calC$ with $s, s' \in S$.
            Then the compositions $fs^{-1}$ and $f'{s'}^{-1}$ in $\calC[S^{-1}]$ are equal if and only if there exists a third pair of maps $x \xleftarrow{s''} z'' \xrightarrow{f''} y$ and maps $p \colon z'' \to z$ and $p' \colon z'' \to z'$ in $\calC$ such that the diagram
            \[
            \begin{tikzcd}[row sep = large, column sep = large]
                &
                z \arrow[swap]{ld}{s} \arrow{rd}{f}
                &
                \\
                x
                &
                z'' \arrow{l}{s''} \arrow[swap]{u}{p} \arrow{d}{p'} \arrow[swap]{r}{f''}
                &
                y
                \\
                &
                z' \arrow{lu}{s'} \arrow[swap]{ru}{f'}
                &
            \end{tikzcd}
            \]
            in commutative.
        \end{itemize}
    \end{lemma}

    \begin{proof}
        This is similar to the material in \cite[\href{https://stacks.math.columbia.edu/tag/04VB}{Tag 04VB}]{stacks-project} but slightly more general as with our definition a right multiplicative system is not assumed to be stable under composition.
    \end{proof}

    \begin{lemma}\label{lem:ind-funct-ff}
        Let $F \colon \calC \to \calD$ be a functor and let $S$ be a right multiplicative system in $\calC$ such that $F(s)$ is an isomorphism in $\calD$ for all $s \in S$.
        Assume that the following conditions are satisfied:
        \begin{itemize}
            \item
            $F$ is faithful.

            \item
            Suppose we are given $x, y \in \calC$ and a map $g \colon F(x) \to F(y)$ in $\calD$.
            Then there exist maps $s \colon z \to x$ and $f \colon z \to y$ in $\calC$ such that $s$ is contained in $S$ and $g = F(f)F(s)^{-1}$.
        \end{itemize}
        Then the resulting functor $\widetilde{F} \colon \calC[S^{-1}] \to \calD$ is fully faithful.
    \end{lemma}

    \begin{proof}
        The first condition implies that we really obtain a functor $\widetilde{F}$ by \cref{def:loc-cats}.
        Also \cref{lem:loc-exists} immediately implies that $\widetilde{F}$ is full.
        To see that $\widetilde{F}$ is also faithful assume that we are given two maps $x \to y$ in $\calC[S^{-1}]$ that map to the same map under $\widetilde{F}$.
        By \cite[\href{https://stacks.math.columbia.edu/tag/04VE}{Tag 04VE}]{stacks-project} (whose proof we have to slightly adjust) we can represent them as $f s^{-1}$ and $f' s^{-1}$ for some maps $f \colon z \to y$, $f' \colon z \to y$ and $s \colon z \to x$ in $\calC$ with $s \in S$.
        It then follows that $\widetilde{F}(f) = \widetilde{F}(f')$ so that we obtain $f = f'$ by faithfulness of $F$.
    \end{proof}

    \section{Rigid geometry via formal schemes}

    \begin{notation}
        For the rest of the talk we fix a complete nonarchimedean (and nontrivially) valued field $K$ with valuation ring $(R, \frakm)$ and pseudo-uniformizer $\pi \in \frakm \setminus \curlybr{0}$.
    \end{notation}

    \subsection{Admissible formal blowing ups and quasiparacompactness}

    Here we give (without proof) some of the material from \cite[Section 8.2.]{bosch-formal-rigid} that we need later on.
    Part of the material was already discussed in Alessandro's talk but we include it as recollection.

    \begin{definition}\label{def:afbup}
        Let $X \in \catfsch(R)^{\adm}$ and let $\calA \subseteq \calO_X$ be a coherent open ideal.
        Then the \emph{admissible formal blowing up of $X$ in $\calA$} is the object $(X_{\calA}, p \colon X_{\calA} \to X) \in (\catfsch(R)^{\adm})_{/X}$ that is uniquely (up to unique isomorphism) determined by the following properties:
        \begin{itemize}
            \item
            $\calA \cdot \calO_{X_{\calA}}$ is an invertible $\calO_{X_{\calA}}$-module.

            \item
            Suppose that $f \colon Y \to X$ is a map of admissible formal $R$-schemes such that $\calA \cdot \calO_Y$ is an invertible $\calO_Y$-module.
            Then there exists a unique map $\tilde{f} \colon Y \to X_{\calA}$ such that $f = p \tilde{f}$.
        \end{itemize}
        A map $p \colon X' \to X$ in $\catfsch(R)^{\adm}$ is called an \emph{admissible formal blowing up} if $(X', p)$ is the admissible formal blowing up of $X$ in some coherent open ideal $\calA \subseteq \calO_X$.
    \end{definition}

    It is not clear from \cref{def:afbup} that admissible formal blowing ups exist.
    This follows from the following Lemma that gives an explicit construction (and the local nature of \cref{def:afbup}):

    \begin{lemma}\label{lem:afbup-affine}
        Let $A \in \catalgs(R)^{\adm}$ and let $f_0, \dotsc, f_r \in A$ (for some $r \in \ZZ_{\geq 0}$) generating an open ideal $\fraka \subseteq A$.
        For $i = 0, \dotsc, r$ set $C_i \coloneqq A\anglebr[\big]{f_j/f_i, \, j = 0, \dotsc, r}$ and let $A_i \coloneqq C_{i, \adm}$ (i.e. the quotient of $C_i$ by its $\pi$-torsion).
        Note that we have isomorphisms $A_i\anglebr[\big]{(f_j/f_i)^{-1}} \cong A_j\anglebr[\big]{(f_i/f_j)^{-1}}$ for $i, j = 0, \dotsc, r$ that satisfy the cocycle condition.

        Then the admissible formal blowing up of $\Spf(A)$ in $\fraka^{\triangle}$ is given by the gluing of the $\Spf(A_i)$ for $i = 0, \dotsc, r$ along the above isomorphisms (together with its projection to $\Spf(A)$).
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.7.]{bosch-formal-rigid}.
    \end{proof}

    \begin{lemma}\label{lem:afbup-compl-square}
        Let $f \colon Y \to X$ be a map in $\catfsch(R)^{\adm}$ and let $p \colon X' \to X$ be an admissible formal blowing up.
        Then exists an admissible formal blowing up $p' \colon Y' \to Y$ and a map $f' \colon Y' \to X'$ such that $p f' = f p'$.
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.16.]{bosch-formal-rigid}.
    \end{proof}

    \begin{lemma}\label{lem:afbup-comp-qc}
        Let $X \in \catfsch(R)^{\adm, \qc}$ and let $p \colon X' \to X$ and $p' \colon X'' \to X'$ be two admissible formal blowing ups.
        Then also the composition $p p'$ is an admissible formal blowing up.
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.11.]{bosch-formal-rigid}.
    \end{proof}

    \begin{definition}
        Let $X \in \catfsch(R)^{\adm}$ (resp.\ $X \in \catrigsp(K)$).
        Then $X$ is called quasiparacompact if there exists an (admissible) open covering $(U_i)_{i \in I}$ such that the following conditions are satisfied:
        \begin{itemize}
            \item
            Each $U_i$ is quasicompact in the sense that every (admissible) open covering of $U_i$ admits a finite (admissible) refinement.

            \item
            The covering $(U_i)_{i \in I}$ is of finite type in the sense that every $U_i$ intersects only finitely many $U_j$ nontrivially.
        \end{itemize}
    \end{definition}

    \begin{lemma}\label{lem:ext-afbup}
        Let $X \in \catfsch(R)^{\adm, \qpc}$ and let $U \subseteq X$ be a quasicompact open.
        Then every formal admissible blowing up $p_U \colon U' \to U$ can be extended to a formal admissible blowing up $p \colon X' \to X$.
        This can even be done in a way such that $p|_{p^{-1}(V)} \colon p^{-1}(V) \to V$ is an isomorphism for all open $V \subseteq X$ that are disjoint from $U$.  
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.13.]{bosch-formal-rigid}.
    \end{proof}

    \begin{lemma}\label{lem:qpc-dom-afbup}
        Let $X \in \catfsch(R)^{\adm, \qpc}$ and let $(U_i)_{i \in I}$ be a covering of $X$ that is of finite type and such that each $U_i$ is quasicompact.
        Also suppose we are given a family $(p_i \colon U'_i \to U_i)_{i \in I}$ of admissible formal blowing ups.
        Then there exists an admissible formal blowing up $p \colon X' \to X$ such that $p|_{p^{-1}(U_i)}$ factors over $p_i$ for all $i \in I$.
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.14.]{bosch-formal-rigid}.
    \end{proof}

    \begin{lemma}\label{lem:afbup-comp}
        Let $X \in \catfsch(R)^{\adm, \qpc}$ and let $p \colon X' \to X$ and $p' \colon X'' \to X'$ be admissible formal blowing ups.
        Then there exists a map $f \colon X''' \to X''$ in $\catfsch(R)^{\adm}$ such that $pp'f$ is again a admissible formal blowing up.
    \end{lemma}

    \begin{proof}
        See \cite[Proposition 8.2.15.]{bosch-formal-rigid}.
    \end{proof}

    \subsection{Raynaud's Theorem}

    The reference for the materal in this subsection is \cite[Section 8.4.]{bosch-formal-rigid}.

    \begin{lemma}\label{lem:rig-afbups-isos}
        The functor $\rig \colon \catfsch(R)^{\adm} \to \catrigsp(K)$ maps admissible formal blowing ups to isomorphisms.
    \end{lemma}

    \begin{proof}
        To prove the claim it suffices to show that admissible formal blowing ups with affine target map to isomorphisms under $\rig$.
        So let $A \in \catalgs(R)^{\adm}$ and let $f_0, \dotsc, f_r \in A$ generate an open ideal $\fraka$.
        Let $X \coloneqq \Spf(A)$ and $\calA \coloneqq \fraka^{\triangle}$.
        For $i = 0, \dotsc, r$ set $A_i \coloneqq A\anglebr[\big]{f_j/f_i, \, j = 0, \dotsc, r}_{\adm}$.
        Note that
        \[
            A_{i,\rig} = A_{\rig}\anglebr[\big]{f_j/f_i, \, j = 0, \dotsc, r}
        \]
        so that the natural map $\Sp(A_{i, \rig}) = \Spf(A_i)_{\rig} \to X_{\rig}$ identifies with the inclusion of a rational domain (note that the $f_j$ generate the unit ideal in $A_{\rig}$).
        Moreover, for $i, j = 0, \dotsc, r$ the intersection $\Sp(A_{i, \rig}) \cap \Sp(A_{j, \rig})$ inside $X_{\rig}$ is given by $\Sp(A_{ij, \rig})$ where we set $A_{ij} \coloneqq A_i\anglebr[\big]{(f_j/f_i)^{-1}}$.
        Thus applying the functor $\rig$ to the gluing datum describing $X_{\calA}$ (from \cref{lem:afbup-affine}) we obtain the rational covering of $X_{\rig}$ generated by the elements $f_i \in A_{\rig}$ so that we win.
    \end{proof}

    Our goal is now to prove the following theorem:

    \begin{theorem}\label{thm:raynaud}
        The functor
        \[
            \rig \colon \catfsch(R)^{\adm, \qpc}[\afbup^{-1}] \to \catrigsp(K)
        \]
        is fully faithful with essential image $\catrigsp(K)^{\qpc, \qs}$.
    \end{theorem}

    Our strategy is to verify the assumptions from \cref{lem:ind-funct-ff} and then to identify the essential image.

    \begin{lemma}\label{lem:rig-faithful}
        The functor $\rig \colon \catfsch(R)^{\adm} \to \catrigsp(K)$ is faithful.
    \end{lemma}

    \begin{proof}
        It suffices to show that $\rig$ is faithful when restricted to the full subcategory of affine admissible formal $R$-schemes.
        But then the claim follows simply because every admissible $R$-algebra $A$ maps injectively into $A_{\rig}$.
    \end{proof}

    \begin{lemma}
        The admissible formal blowing ups in $\catfsch(R)^{\adm, \qpc}$ form a right multiplicative system.
    \end{lemma}

    \begin{proof}
        The first condition is clear.
        The second and third conditions are precisely \cref{lem:afbup-comp} and \cref{lem:afbup-compl-square}.
        The fourth condition follows from \cref{lem:rig-faithful} because it implies that two parallel maps in $\catfsch(R)^{\adm}$ are equal as soon as they get equalized by an admissible formal blowing up.
        Finally the last condition holds true because every admissible formal blowing up of an object $X \in \catfsch(R)^{\adm}$ is given by some coherent open ideal $\calA \subseteq \calO_X$ and there is only a set of those.
    \end{proof}

    To verify the last condition from \cref{lem:ind-funct-ff} we need two preliminary lemmas.

    \begin{lemma}\label{lem:open-cov-rig}
        Let $X \in \catfsch(R)^{\adm, \qpc}$ and let $\calU_K$ be an admissible open covering of $X_{\rig}$.
        Then there exists an admissible formal blowing up $p \colon X' \to X$ and an affine open covering $\calU'$ of $X'$ of finite type such that the admissible open covering $\rig(p)(\calU'_{\rig})$ of $X_{\rig}$ is a refinement of $\calU_K$.
    \end{lemma}

    \begin{proof}
        First assume that $X = \Spf(A)$ is affine.
        Then, using \cite[Lemma 4.3.4.]{bosch-formal-rigid} (from Paulina's talk), we may assume that $\calU_K$ is a rational covering generated by elements $f_0, \dotsc, f_r \in A_{\rig}$ that generate the unit ideal.
        After multiplying the $f_i$ by a large power of $\pi$ we may assume that they are all contained in $A$.
        Then they generate an open ideal $\fraka \subseteq A$ and we can set $X' \coloneqq X_{\calA}$ for $\calA \coloneqq \fraka^{\triangle}$.
        The open covering of $X'$ from \cref{lem:afbup-affine} then precisely gives back the covering $\calU_K$ after applying $\rig$ as we have seen in the proof of \cref{lem:rig-afbups-isos}.

        In the general case pick an affine open covering $X = \bigcup_{i \in I} U_i$ of finite type (this is possible as $X$ is quasiparacompact).
        Then we can apply the above to each of the $U_i$ and the restriction $\calU_K|_{U_{i, \rig}}$ and obtain admissible formal blowing ups $p_i \colon U'_i \to U_i$ and finite open coverings $\calU'_i$ of $U'_i$ by affine opens such that the rigidification of each element of $\calU'_i$ maps into some element of $\calU_K$.
        Applying \cref{lem:qpc-dom-afbup} we find an admissible formal blowing up $p \colon X' \to X$ such that $p|_{p^{-1}(U_i)}$ factors over $p_i$ via some map $q_i \colon p^{-1}(U_i) \to U'_i$ for all $i \in I$.
        Defining
        \[
            \calU' \coloneqq \set[\Big]{q_i^{-1}(V)}{i \in I, \; V \in \calU'_i}
        \]
        now works except for that the elements of $\calU'$ are not necessarily affine.
        But we can replace every element of $\calU'$ by an affine open covering and then realize that for each $i \in I$ finitely many of these suffice to cover $p^{-1}(U_i)$ as it is quasicompact.
        This gives the desired affine open covering of finite type.
    \end{proof}

    \begin{remark}
        If in \cref{lem:open-cov-rig} we assume that the covering $\calU_K$ itself is of finite type and consists of quasicompact admissible opens then we even can arrange that every element of $\calU_K$ can be written as a (finite) union of elements in $\rig(p)(\calU'_{\rig})$.
        This form of the Lemma will also be needed later.
    \end{remark}

    \begin{lemma}\label{lem:fin-afbup}
        Let $A \in \catalgs(R)^{\adm}$ and let $f_1, \dotsc, f_n \in A_{\rig}$ such that $\abs{f_i}_{\sup} \leq 1$ far $i = 1, \dotsc, n$.
        Then $A' \coloneqq A[f_1, \dotsc, f_n] \subseteq A_{\rig}$ is an admissible $R$-algebra that is finite over $A$.
        The canonical map $\Spf(A') \to \Spf(A)$ is the formal admissible blowing up of $\Spf(A)$ in the ideal $\calA \coloneqq \fraka^{\triangle}$ where $\fraka \coloneqq (c, cf_1, \dotsc, cf_n)$ and $c \in \frakm \setminus \curlybr{0}$ is chosen such that all the $cf_i$ are contained in $A$.
    \end{lemma}

    \begin{proof}
        Pick a surjection $R\anglebr[\big]{x_1, \dotsc, x_m} \to A$ inducing a surjection $K\anglebr[\big]{x_1, \dotsc, x_m} \to A_{\rig}$ that gives rise to a residue norm $\abs{\blank} \colon A_{\rig} \to \RR_{\geq 0}$.
        It follows from \cite[Proposition 3.1.5.(iii)]{bosch-formal-rigid} (from Bence's talk) that
        \[
            A = \set[\big]{a \in A_{\rig}}{\abs{a} \leq 1}.
        \]
        From \cite[Theorem 3.1.17.]{bosch-formal-rigid} (that I think was not covered in the seminar) we thus see that the $f_i$ are integral over $A$ so that $A'$ is a finite $A$-algebra.
        Now choose $c$ as in the statement of the Lemma.
        Then we have $cA' \subseteq A$ so that it follows that $A'$ is $\pi$-complete.
        Thus the map
        \[
            R\anglebr[\big]{x_1, \dotsc, x_m, y_1, \dotsc, y_n} \to A', \qquad y_i \mapsto f_i
        \]
        is a well-defined surjection showing that $A'$ is an $R$-algebra of tft.
        As $A'$ is also flat over $R$ we can conclude that it is admissible (using \cite[Corollary 7.3.5.]{bosch-formal-rigid} from Pietro's talk).

        Now it remains to show that $\Spf(A') \to \Spf(A)$ is the admissible formal blowing up of $\Spf(A)$ in the ideal $\calA$.
        For this we argue directly by \cref{def:afbup}:
        \begin{itemize}
            \item
            We have that $\fraka \cdot A' = (c)$ so that it is an invertible $A'$-module.

            \item
            Suppose that $Y \to \Spf(A)$ is a map in $\catfsch(R)^{\adm}$ such that $\calA \cdot \calO_Y$ is an invertible $\calO_Y$-module.
            Then we have to show that this map factors uniquely over $\Spf(A') \to \Spf(A)$.
            As the uniqueness is clear from \cref{lem:rig-faithful} we may show existence of the factorisation locally on $Y$ and hence we may assume that $Y = \Spf(B)$ is affine and that $\fraka \cdot B$ is generated by either $c$ or one of the $c f_i$.
            We then have to show that the elements $f_i \in B_{\rig}$ are actually contained in $B$.
            \begin{itemize}
                \item
                Assume first that $\fraka \cdot B = (c)$.
                Then $c f_i = b_i c$ for some $b_i \in B$ and as $c$ is a nonzerodivisor in $B$ we can conclude that $f_i = b_i \in B$.
                
                \item
                Now assume that $\fraka \cdot B = (c f_i)$ for some $i$.
                Then we have $c = b c f_i$ for some $b \in B$.
                From this we see that $f_i$ is a unit in $B_{\rig}$ and that $f_i^{-1} \in B$.
                As $f_i$ is also integral over $B$ we find an equation
                \[
                    f_i^s + b_1 f_i^{s - 1} + \dotsb + b_s = 0.
                \]
                with $b_j \in B$ for $j = 1, \dotsc, s$.
                Multiplying with $f_i^{s - 1}$ we obtain
                \[
                    f_i = - (b_1 + \dotsb + b_s f_i^{1 - s}) \in B.
                \]
                Thus $f_i \in B^{\times}$ so that $\fraka \cdot B = (c)$ and we win. \qedhere
            \end{itemize}
        \end{itemize}
    \end{proof}

    \begin{lemma}\label{lem:rig-full}
        Let $X, Y \in \catfsch(R)^{\adm}$ and assume that $X$ is quasiparacompact.
        Let $g \colon X_{\rig} \to Y_{\rig}$ be a map in $\catrigsp(K)$.
        Then there exists an admissible formal blowing up $p \colon X' \to X$ and a map $f \colon X' \to Y$ such that $f = \rig(f) \rig(p)^{-1}$.
    \end{lemma}

    \begin{proof}
        Assume first that $X = \Spf(A)$ and $Y = \Spf(B)$ are both affine.
        Then $g$ is given by a map of affinoid $K$-algebras $B_{\rig} \to A_{\rig}$.
        Choose a surjection $R\anglebr[\big]{x_1, \dotsc, x_n} \to B$ and set $f_i \in A_{\rig}$ to be the image of $x_i$ for $i = 1, \dotsc, n$.
        Then we have $\abs{f_i}_{\sup} \leq 1$ for all $i$ so that we can apply \cref{lem:fin-afbup} to see that $A' \coloneqq A[f_1, \dotsc, f_n]$ is an admissible $R$-algebra and that $\Spf(A') \to \Spf(A)$ is a formal admissible blowing up.
        Now the map $B_{\rig} \to A_{\rig}$ clearly restricts to a map $B \to A'$ so that we win.

        In the general case choose an affine open covering $\calV$ of $Y$ and set
        \[
            \calU_K \coloneqq \set[\Big]{g^{-1}(V_{\rig})}{V \in \calV}.
        \]
        Then we can apply \cref{lem:open-cov-rig} to the admissible open covering $\calU_K$ of $X_{\rig}$ to obtain an admissible formal blowing up $p \colon X' \to X$ and an affine open covering $\calU'$ of $X'$ of finite type such that $p_{\rig}(\calU'_{\rig})$ is a refinement of $\calU_K$.
        For every $U \in \calU'$ choose $V_U \in \calV$ such that $U_{\rig}$ maps into $V_{U, \rig}$ under $g$.
        Then, applying the first part of the proof we find an admissible formal blowing up $p_U \colon U' \to U$ and a map $f_U \colon U' \to V$ such that
        \[
            \rig(f_U) \rig(p_U)^{-1} = g \rig\roundbr[\big]{p|_U}.
        \]
        Using \cref{lem:qpc-dom-afbup} we find an admissible formal blowing up $p' \colon X'' \to X'$ such that $p'|_{{p'}^{-1}(U)}$ factors over $p_U$ via a map $q_U \colon {p'}^{-1}(U) \to U'$ for all $U \in \calU'$.
        By \cref{lem:rig-faithful} the maps $f_U q_U \colon {p'}^{-1}(U) \to Y$ agree on the overlaps and thus glue to give a well-defined map $f \colon X'' \to Y$.
        Finally we apply \cref{lem:afbup-comp} to the admissible formal blowing ups $X'' \xrightarrow{p'} X' \xrightarrow{p} X$ and win (at least after renaming).
    \end{proof}

    At this point we have proven the fully faithfulness of \cref{thm:raynaud}.
    It remains to identify the essential image.
    That the essential image is contained in $\catrigsp(K)^{\qpc, \qs}$ is clear (note that every admissible formal $R$-scheme is automatically quasiseparated).
    Showing the other inclusion will occupy us for the rest of the talk.

    \begin{lemma}\label{lem:iso-afbups}
        Let $X, Y \in \catfsch(R)^{\adm, \qc}$ and let $g \colon X_{\rig} \to Y_{\rig}$ be an isomorphism.
        Then there exist formal admissible blowing ups $p_X \colon Z \to X$ and $p_Y \colon Z \to Y$ such that $g = \rig(p_Y) \rig(p_X)^{-1}$.
    \end{lemma}

    \begin{proof}
        Using \cref{lem:rig-full} we find formal admissible blowing ups $p'_X \colon X' \to X$ and $p'_Y \colon Y' \to Y$ and two maps $f_X \colon X' \to Y$ and $f_Y \colon Y' \to X$ such that $g = \rig(f_X) \rig(p'_X)^{-1}$ and $g^{-1} = \rig(f_Y) \rig(p'_Y)^{-1}$.
        Let $\calA \subseteq \calO_X$ and $\calB \subseteq \calO_Y$ be coherent open ideals such that $p'_X$ and $p'_Y$ are admissible formal blowing ups in $\calA$ and $\calB$.
        Then we can further consider the formal admissible blowing ups $p''_X \colon X'' \coloneqq X'_{(\calA \cdot \calO_{X'}) \cdot (\calB \cdot \calO_{X'})} \to X'$ and $p''_Y \colon Y'' \coloneqq Y'_{(\calA \cdot \calO_{Y'}) \cdot (\calB \cdot \calO_{Y'})} \to Y'$.

        As $\calB \cdot \calO_{X''}$ is an invertible $\calO_{X''}$-module the composition $X'' \xrightarrow{p''_X} X' \xrightarrow{f_X} Y$ factors over $p'_Y$ via a map $f'_X \colon X'' \to Y'$.
        By \cref{lem:rig-faithful} we have $p'_X p''_X = f_Y f'_X$.
        Now also
        \[
            \roundbr[\Big]{(\calA \cdot \calO_{Y'}) \cdot (\calB \cdot \calO_{Y'})} \cdot \calO_{X''} = \roundbr[\Big]{(\calA \cdot \calO_{X'}) \cdot (\calB \cdot \calO_{X'})} \cdot \calO_{X''}
        \]
        is an invertible $\calO_{X''}$-module so that the map $f'_X$ factors over $p''_Y$ via a map $f''_X \colon X'' \to Y''$.

        Similarly we also obtain a map $f''_Y \colon Y'' \to X''$ and again using \cref{lem:rig-faithful} we see that $f''_X$ and $f''_Y$ are mutually inverse isomorphisms.
        Hence we may set $Z \coloneqq X'' = Y''$ and $p_X \coloneqq p'_X p''_X$ and $p_Y \coloneqq p'_Y p''_Y$ and conclude using \cref{lem:afbup-comp-qc} to see that $p_X$ and $p_Y$ are indeed admissible formal blowing ups.
    \end{proof}

    \begin{lemma}
        Let $X_K \in \catrigsp(K)^{\qpc, \qs}$.
        Then there exists $X \in \catfsch(R)^{\adm, \qpc}$ and an isomorphism $X_{\rig} \cong X_K$.
    \end{lemma}

    \begin{proof}
        First assume that $X_K = \Sp(A_K)$ is affinoid.
        Then we choose a surjection $K\anglebr[\big]{x_1, \dotsc, x_n} \to A_K$ and set $A$ to be the image of $R\anglebr[\big]{x_1, \dotsc, x_n}$ under this map.
        Using \cite[Corollary 7.3.5.]{bosch-formal-rigid} (from Pietro's talk) we see that $A$ is an admissible $R$-algebra.
        Moreover we clearly have $A_K = A_{\rig}$ so we win.

        Next assume that $X_K$ is quasicompact so that it has an admissible open covering by finitely many affinoids.
        We proceed by induction with the base case handled by the first part of the proof.
        So assume that we have an admissible open covering $X_K = U_{1, K} \cup U_{2, K}$, where the $U_{i, K}$ are quasicompact, and $U_1, U_2 \in \catfsch(R)^{\adm, \qc}$ with isomorphisms $U_{i, K} \cong U_{i, \rig}$.
        The intersection $W_K \coloneqq U_{1, K} \cap U_{2, K}$ is quasicompact because $X_K$ is quasiseparated.
        Thus we may apply \cref{lem:open-cov-rig} (or rather its refined version) to obtain admissible formal blowing ups $U'_i \to U_i$ and opens $W_i \subseteq U'_i$ such that the $W_{i, \rig}$ both identify with $W_K$.
        Using \cref{lem:iso-afbups} we obtain admissible formal blowing ups $W \to W_i$ and using \cref{lem:ext-afbup} we can extend these to admissible formal blowing ups $U''_i \to U'_i$.
        Finally we then can define $X$ to be the gluing of $U''_1$ and $U''_2$ along $W$.

        Now consider the general case.
        Pick an admissible open covering $X_K = \bigcup_{i \in I} X_{i, K}$ of finite type by quasicompact admissible opens.
        Pick $i_0 \in I$ and set $U_{0, K} \coloneqq X_{i_0, K}$.
        Now we recursively define
        \[
            U_{n + 1, K} \coloneqq \bigcup \set[\Big]{X_{i, K}}{X_{i, K} \cap U_{n, K} \neq \emptyset, \; X_{i, K} \not\subseteq U_{0, K} \cup \dotsb \cup U_{n, K}}.
        \]
        We may now without loss of generality assume that $X_K = \bigcup_{n = 0}^{\infty} U_{n, K}$ (indeed repeating the above construction for varying $i_0$'s gives a decomposition of $X_K$ as a disjoint union of opens).
        Then the $U_n$ form an admissible covering of $X_K$ and each $U_n$ is quasicompact.
        Set $V_{n, K} \coloneqq U_{0, K} \cup \dotsb \cup U_{n, K}$.

        We now inductively construct $V_n \in \catfsch(R)^{\adm, \qc}$ together with open coverings $V_n = U_0 \cup \dotsb \cup U_n$ and isomorphisms $V_{n, \rig} \cong V_{n, K} \coloneqq U_{0, K} \cup \dotsb \cup U_{n, K}$ restricting to isomorphisms $U_{i, \rig} \cong U_{i, K}$ and open immersions $V_n \subseteq V_{n + 1}$ that are compatible with the inclusions $V_{n, K} \subseteq V_{n + 1, K}$.
        \begin{itemize}
            \item
            We define $V_0 = U_0$ to be any (quasicompact) admissible formal $R$-scheme with an isomorphism $V_{0, \rig} \cong V_{0, K}$.
            This exists by the second step of the proof.

            \item
            Now assume that we have already constructed the above data for some $n$.
            Then we can pick some $U_{n + 1} \in \catfsch(R)^{\adm, \qc}$ together with an isomorphism $U_{n + 1, \rig} \cong U_{n + 1, K}$.
            Similarly to what we have done in the second step of the proof we can replace $U_{n + 1}$ by a suitable admissible formal blowing up and find an admissible formal blowing up $U'_n \to U_n$ such that we can glue $U_{n + 1}$ and $U'_n$ compatibly.
            Using \cref{lem:ext-afbup} we can extend the admissible formal blowing up $U'_n \to U_n$ to an admissible formal blowing up $V'_n \to V_n$ that is an isomorphism over $V_{n - 2}$ (where we set $V_{-1} \coloneqq \emptyset$).
            Now we can replace $V_n$ by $V'_n$ (and also replace $V_{n-1}$ accordingly) and define $V_{n + 1}$ as the gluing of $V_n$ and $U_{n + 1}$.
        \end{itemize}
        Finally we can define $X$ as the gluing of all the $V_n$ and win.
    \end{proof}

    \printbibliography
\end{document}